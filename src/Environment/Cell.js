import React from "react";
import styled from "styled-components";

const StyledCell = styled.span`
  padding: 4px;
  width: 20px;
  display: inline-block;
`;

const Cell = (props) => {
  return <StyledCell>{props.children}</StyledCell>
}

export default Cell;