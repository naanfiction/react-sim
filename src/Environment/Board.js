import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {renderEntity} from "../entities/EntityFactory";
import Cell from "./Cell";

const RowBorders = styled.div`
  margin: 0 10px;
`;

function Row(props) {
  return  (
    <RowBorders>
      {props.row.map((entity, ind) => {
        return <Cell key={ind}>{renderEntity(entity)}</Cell>;
      })}
    </RowBorders>
  );
}

function Board({height, width, cells}) {
  let lines = [];
  for (var i = 0; i < height; i++) {
    var startInd = i * width;
    var endInd = startInd + width;
    // line starts with index mod 0, ends with index mod width - 1
    lines.push(cells.slice(startInd, endInd));
  }
  return lines.map((line, ind) => {
    return <Row key={ind} row={line} />;
  });
};

Board.defaultProps = {
  height: 0,
  width: 0,
  cells: []
};

Board.propTypes = {
  height: PropTypes.number,
  width: PropTypes.number,
  cells: PropTypes.array
};

export { Board, Cell };
