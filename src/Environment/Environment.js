import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {Board} from './Board'
import {integerDivide, integerModulus } from "../supp/Utils";
import { getRandomIntToMax } from '../supp/Utils';
const ENTITY_TYPES = require('../entities/EntityTypes');


// Environment has board and creatures
class Environment extends Component {
    
    constructor(props) {
        super(props);
        this.validateParameters();
        var totalWidth = props.width + 2;
        var totalHeight = props.height + 2;
        var {cells, entities} = this.initializeBoard(totalHeight, totalWidth);
        this.state = {name: props.name, height: totalHeight, width: totalWidth, cells: cells, entities: entities};
    }

    
    // placeInEnvironment = (entity, location) => {
    //     console.log("## trying to place entity at location: ", entity, location);
    //     var cells = this.state.cells.slice();
    //     var entities = this.state.entities.slice();
    //     console.log("## copy of current state is: ", cells);
    //     entities.push(entity);
    //     cells[location] = entity;
    //     this.setState({cells, entities});
    // }

    initializeBoard = (height, width) => {
        var returned = {entities: [], cells: []}
        returned.cells = this.initializeWalls(height, width);
        this.initializePopulation(returned);
        return returned;
    }

    initializeWalls = (height, width) => {
        var walls = [];
        for (var i = 0; i < height * width; i++) {
            var quotient = integerDivide(i, width);
            var remainder = integerModulus(i, width);
            
            // if last row or first row, fill in entire row
            if ((quotient === 0 || quotient === height - 1) || (remainder === 0 || remainder === width - 1)) {
                walls.push(ENTITY_TYPES.WALL);
            } else {
                walls.push(ENTITY_TYPES.EMPTY);
            }
        }
        return walls;
    }

    get numSquares() {
        return this.state.height * this.state.width;
    }

    // isLocationOccupied = (location) => {
    //     return this.state.cells[location] !== ENTITY_TYPES.EMPTY;
    // }

    // populateSpecies = (type, numToPlace) => {
    //     console.log("# populating. type is: ", type, numToPlace);
    //     var numberOfThingsPlaced = 0;
    //     for (var i = 0; numberOfThingsPlaced < numToPlace; i++) {
    //       var randIndex = getRandomIntToMax(this.numSquares);
    //       console.log("## random index is: ", randIndex);
    //       if (!this.isLocationOccupied(randIndex)) {
    //         console.log("## not occupied, placing");
    //         // var entity = ENTITY_FACTORY.createNewEntity(type);
    //         this.placeInEnvironment(type, randIndex);
    //         // self.entities.push(entity);
    //         // self.environment.placeInEnvironment(entity, randIndex);
    //         numberOfThingsPlaced += 1;
    //       }
    //     }

    //     console.log("## tried to set state. now it is: ", this.state.cells);
    //     console.log("## tried to set state. now it is: ", this.state.entities);
    //   }

    initializeSpecies = (cells, entities, type, numToPlace) => {
        var numberOfThingsPlaced = 0;
        for (var i = 0; numberOfThingsPlaced < numToPlace; i++) {
          var randIndex = getRandomIntToMax(cells.length);
          var occupied = cells[randIndex] !== ENTITY_TYPES.EMPTY;
          if (!occupied) {
            entities.push(type);
            cells[randIndex] = type;
            numberOfThingsPlaced += 1;
          }
        }
    }

    initializePopulation = ({entities, cells}) => {
        let numCreaturesMap = {[ENTITY_TYPES.WANDERER]: 1};

        this.initializeSpecies(cells, entities, ENTITY_TYPES.WANDERER, numCreaturesMap[ENTITY_TYPES.WANDERER]);
    }

    // populateWorld = () => {
    //     let numCreaturesMap = {[ENTITY_TYPES.WANDERER]: 1};

    //     // this.populateSpecies(ENTITY_TYPES.PLANT, numCreaturesMap);
    //     this.populateSpecies(ENTITY_TYPES.WANDERER, numCreaturesMap[ENTITY_TYPES.WANDERER]);
    // }

    render() {
        return (
            <div>
                <p>Name: {this.state.name}</p>
                <Board height={this.state.height} width={this.state.width} cells={this.state.cells}></Board>
            </div>
        )   
    }

    validateParameters = () => {
        var newHeight = this.props.height + 2, newWidth = this.props.width + 2;
        if (this.props.cells && this.props.cells.length !== newHeight * newWidth) {
            throw new Error("Walls size does not match width and height");
        }
    }
}

Environment.defaultProps = {
    name: "Default Name",
    height: 0, 
    width:0,
    cells: null
}

Environment.propTypes = {
    name: PropTypes.string,
    height: PropTypes.number,
    width: PropTypes.number,
    cells: PropTypes.array,
}

/* Keep, but not using yet 

function addWallsToEnvironment(wallLocations) {
    // If walls are not provided, create a simle border wall.  otherwise use whatever
    // walls we are given
    self.environment = wallLocations;
    if (!wallLocations) {
      self.environment = createBorderWalls();
    }
}
*/


export default Environment;
