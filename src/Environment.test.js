import React from 'react';
import ReactDOM from 'react-dom';
import Environment from './environment/Environment';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Environment />, div);
  ReactDOM.unmountComponentAtNode(div);
});
