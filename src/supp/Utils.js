function integerDivide(numerator, denominator) {
  return Math.floor(numerator/denominator)
}

function integerModulus(numerator, denominator) {
  return Math.floor(numerator) % Math.floor(denominator);
}

function getRandomIntToMax(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

export {integerDivide, integerModulus, getRandomIntToMax}
// module.exports = {
//   integerDivide: integerDivide,
//   integerModulus: integerModulus,
//   getRandomIntToMax: getRandomIntToMax
// }
