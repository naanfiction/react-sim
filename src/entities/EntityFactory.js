import React from "react";
import {Wall, Empty, Wanderer} from "./index";
const ENTITY_TYPES = require('./EntityTypes');

const components = {
  [ENTITY_TYPES.EMPTY]: Empty,
  [ENTITY_TYPES.WALL]: Wall,
  [ENTITY_TYPES.WANDERER]: Wanderer,
}

function renderEntity(type) {
  const SpecificEntity = components[type];
  return <SpecificEntity/>
}

export {renderEntity}