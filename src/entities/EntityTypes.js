module.exports = {
  EMPTY: 'EMPTY',
  WALL: 'WALL',
  WANDERER: 'WANDERER',
  PLANT: 'PLANT'
}
